// Чому для роботи з input не рекомендується використовувати клавіатуру?
// Для роботи з input клавіатура може бути корисною, проте клавіатура може обмежити можливості вводу даних, вона не зможе передати датчики або джойстик, що дозволяє користувачам взаємодіяти з реальними об'єктами. Тож, для більшої ефективності та гнучкості вводу даних можна використовувати додаткові пристрої вводу, такі як миша, джойстик, датчики руху та голосові команди.

const buttons = document.querySelectorAll('.btn');
 window.addEventListener('keydown', event => {
    buttons.forEach((button)=>{
      if(button.textContent.toLowerCase()===event.key.toLowerCase()){
      
        button.style.backgroundColor = '#0077FF';
        button.style.color = '#fff';
      }else
        if(button.textContent.toLowerCase()!== event.key.toLowerCase()){
          button.style.backgroundColor =  '#000000';
       
        }
    })
  });
